Contributing
============

Contributions are welcome, and they are greatly appreciated! Every
little bit helps, and credit will always be given.

Types of Contributions
----------------------

Security issues
---------------
If you discover any security issue, please report it
directly to rodare@hzdr.de (specifically **do not** report it to our GitLab
repository). Please include the the word ``SECURITY`` in all-caps in the email
subject line. We will acknowledge the issue as fast as possible, and keep you
updated on the progress of fixing the issue. We additional ask you
to not publicise the issue until after we have fixed it. After we have fixed
the issue we will publicly open and close an issue in our GitLab repository to
document the issue and publicly credit you for finding it (with your permission
of course).

Report Bugs
~~~~~~~~~~~

Spread the word
---------------
Happy with Rodare? Then you can help us spread the word. Tell your colleagues, tweet about, show case your data on Rodare. We have limited outreach efforts so any help you can provide is greatly appreciated. If you'd like to present Rodare, feel free to contact us to get inspiration and help. Want us to present, we're happy to give remote webinars if you can gather an audience.

Co-develop Rodare
-----------------
Are you missing a feature in Rodare? Why not help us built the feature. We are
happy to receive both tiny and large contributions to Rodare. Have a look at
our :ref:`development_process` and :ref:`project_life_cycle` and don't hesitate to get in
contact with us.

Contributor agreement
~~~~~~~~~~~~~~~~~~~~~
By making a pull request against our repository, we assume that you agree to
license your contribution under `GPLv3 (source code) <https://gitlab.hzdr.de/fwcc/rodare/rodare/blob/master/LICENSE>`_ / `Creative Commons
Attribution 4.0 International (text content) <https://creativecommons.org/licenses/by/4.0/>`_.
