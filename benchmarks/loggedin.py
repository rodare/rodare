# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Logged-in benchmarks."""

from __future__ import absolute_import, print_function

from anonymous import AnonymousWebsiteTasks
from locust import HttpLocust


class LoggedInWebsiteTasks(AnonymousWebsiteTasks):
    """Benchmark logged-in user."""

    def on_start(self):
        """Load user on start."""
        with self.client.post('https://sandbox.zenodo.org/login/',
                              {
                                  'email': 'test@zenodo.org',
                                  'password': '123456',
                              }) as response:
            if response.status_code != 200:
                response.failure('wrong login')


class WebsiteUser(HttpLocust):
    """Locust."""

    task_set = LoggedInWebsiteTasks
    min_wait = 5000
    max_wait = 15000
