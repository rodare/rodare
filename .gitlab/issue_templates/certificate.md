# Update of weberver and Shibboleth certificate

A complete manual is in our [Rodare deployment docs](https://rodare.pages.hzdr.de/rodare-deployment/maintenance.html#update-certificates).

## rodare4, rodare5 and rodare6

### Webserver

- [ ] Request new webserver certificate for `rodare.hzdr.de`
- [ ] Copy certificate to `/etc/ssl/certs/rodare.hzdr.de.crt`
- [ ] Copy key to `/etc/ssl/private/rodare.hzdr.de.key`
- [ ] Restart nginx with ´service nginx restart´
- [ ] Check certificate in browser and add the **complete certificate chain** to the certificate
- [ ] Create `/etc/ssl/private/rodare.hzdr.de.includesprivatekey.pem` for HAProxy with the **complete certificate chain**
- [ ] Restart ´service haproxy restart´

### Shibboleth

The same cerificate and key is also used for the Shibboleth SP:

- [ ] Rename and copy the same certificate to `/opt/rodare/var/instance/shibboleth/settings/certs/sp_new.crt`
- [ ] Rename and copy the key to `/opt/rodare/var/instance/shibboleth/settings/certs/sp_new.key` 
- [ ] Replace the valid until date `"metadataValidUntil": "2022-02-27T11:59:10Z"` in `/opt/rodare/var/instance/shibboleth/settings/advanced_settings.json`
- [ ] Restart the rodare service and check the metadata at [rodare.hzdr.de/shibboleth/metadata/hzdr](https://rodare.hzdr.de/shibboleth/metadata/hzdr)
- [ ] After a few hours: rename `sp_new.key`and `sp_new.crt` to `sp.key`and `sp.crt`
- [ ] Restart Rodare servcie 

