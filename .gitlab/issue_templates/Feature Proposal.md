### Short description

(Describe your feature proposal concisely.)

### Proposal

(Describe your proposal in detail. How should it work? How could other user benefit from this feature? ...)

### Additional material

(Provide screenshots or material that describe your feature in more detail, if any.)

/label ~"Feature Proposal"