# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Utilities for SIPStore module."""

from __future__ import absolute_import, unicode_literals

import arrow
from invenio_sipstore.archivers.utils import chunks


def generate_bag_path(recid, iso_timestamp):
    """Generates a path for the BagIt.

    Splits the recid string into chunks of size 3, e.g.:
    generate_bag_path('12345', '2017-09-15T11:44:24.590537+00:00') ==
        ['123', '45', 'r', '2017-09-15T11:44:24.590537+00:00']

    :param recid: recid value
    :type recid: str
    :param iso_timestamp: ISO-8601 formatted creation date (UTC) of the SIP.
    :type iso_timestamp: str
    """
    recid_chunks = list(chunks(recid, 3))
    return recid_chunks + ['r', iso_timestamp, ]


def archive_directory_builder(sip):
    """Generate a path for BagIt from SIP.

    :param sip: SIP which is to be archived
    :type SIP: invenio_sipstore.models.SIP
    :return: list of str
    """
    iso_timestamp = arrow.get(sip.model.created).isoformat()
    recid = sip.model.record_sips[0].pid.pid_value
    return generate_bag_path(recid, iso_timestamp)


def sipmetadata_name_formatter(sipmetadata):
    """Generator for the archived SIPMetadata filenames."""
    return "record-{name}.{format}".format(
        name=sipmetadata.type.name,
        format=sipmetadata.type.format
    )
