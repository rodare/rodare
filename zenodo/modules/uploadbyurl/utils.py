# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Customized utility functions of uploadbyurl."""

from __future__ import absolute_import, print_function

import json
import stat
from io import StringIO

import paramiko
import requests
from flask import current_app, flash
from flask_babelex import lazy_gettext as _
from flask_login import current_user
from invenio_db import db
from invenio_uploadbyurl.models import SSHKey
from invenio_uploadbyurl.utils import generate_public_keystr


def store_public_key_fes(username, pub_key, action=''):
    """Store ssh public key for FES via Web API."""
    json_req = dict(
        authentication_type='publickey::rsa',
        configuration=dict(
            user=username,
            pwd=current_app.config['UPLOADBYURL_FES_SECRET'],
            public_key=pub_key,
            action=action,
        )
    )
    current_app.logger.warning('Send request:', str(json.dumps(json_req)), ' to ', str(current_app.config['ZENODO_FES_API']))
    params = {'cjson': json.dumps(json_req)}
    r = requests.post(
        current_app.config['ZENODO_FES_API'],
        params=params,
    )
    current_app.logger.warning('Received :', str(r))
    r.raise_for_status()


def deploy_ssh_key_fes(pub_key, server, username, password, prv):
    """Deploy SSH key to remote machine."""
    with paramiko.SSHClient() as client:
        client.load_system_host_keys()
        current_app.logger.warning('Connect to ', str(server), ' for user ', str(username), ' via password.')
        client.connect(server, username=username, password=password)
        client.close()
        current_app.logger.warning(str('Deploy public ssh key for user ' + str(username) + ' on ' + str(server)))
        try:
            store_public_key_fes(username, pub_key)
        except requests.exceptions.RequestException:
            error_msg = 'Could not store public key on fes for ' \
                        '{user}'.format(user=username)
            flash(_('Could not store public key on fes. Please '
                    'contact us.'), category='danger')
            current_app.logger.error(error_msg)


    with paramiko.SSHClient() as client:
        client.load_system_host_keys()

        pkey = current_app.config[
        'UPLOADBYURL_KEY_DISPATCH_TABLE']['rsa'].from_private_key(
            StringIO(prv))
        client.connect(server, username=username, pkey=pkey)
        client.close()
        return
        '''
        try:
            sftp.chdir(home)
        except IOError:
        # emulate mkdir -p
        try:
            sftp.chdir(home + '/.ssh')
        except IOError:
            try:
                sftp.mkdir(home + '/.ssh')
            except IOError:
                error_msg = 'Could not create .ssh directory for user' \
                            '{user}'.format(user=username)
                flash(_('Could not create .ssh directory. '
                        'Copy the key manually.'), category='danger')
                current_app.logger.error(error_msg)
                return
        stdin, stdout, stderr = client.exec_command(
            'echo "%s" >> ~/.ssh/authorized_keys' % pub_key)
        status = stdout.channel.recv_exit_status()
        if status:
            error_msg = 'Could not add the SSH public to authorized_keys ' \
                        'for user {user}'.format(user=username)
            flash(_('Could not add the SSH public key to authorized_keys. '
                    'Add it manually.'), category='danger')
            current_app.logger.error(error_msg)
            return
        # set correct permissions
        try:
            sftp.chmod(home + '/.ssh/authorized_keys',
                stat.S_IRUSR | stat.S_IWUSR)
        except IOError:
            error_msg = 'Could not set correct access_right for ' \
                        '~/.ssh/authorized_keys, user={user}'.format(
                            user=username)
            flash(_('Could not set correct access rights '
                    'for ~/.ssh/authorized_keys.'), category='danger')
            current_app.logger.error(error_msg)
            return
        try:
            sftp.chmod(home + '/.ssh/', stat.S_IRWXU)
        except IOError:
            error_msg = 'Could not set correct access_right for ' \
                        '~/.ssh/., user={user}'.format(
                            user=username)
            flash(_('Could not set correct access '
                    'rights for directory ~/.ssh/.'), category='danger')
            current_app.logger.error(error_msg)
            return
        '''

def delete_ssh_key_fes(remote):
    """Delete SSH key from remote machine."""
    # TODO: add specitic function to delete key via 'ZENODO_FES_API'
    # retrieve SSH key
    key = SSHKey.get(current_user.id, remote.id)
    if not key:
        flash('Nothing deleted, connection was not setup before.',
              category='warning')
        return
    pub_key = generate_public_keystr(key)
    try:
        store_public_key_fes(key.username, pub_key, action='remove')
    except requests.exceptions.RequestException:
        error_msg = 'Could not delete public key from fes for ' \
                    '{user}'.format(user=key.username)
        flash(_('Could not delete public key from fes. Please '
                'contact us.'), category='danger')
        current_app.logger.error(error_msg)
    # delete key from db
    SSHKey.delete(user_id=current_user.id, remote_server_id=remote.id)
    current_app.logger.warning(str('SSHkey for ' + str(key.username) + ' on ' + str(remote.name) + ' deleted.'))
    db.session.commit()
