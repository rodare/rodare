# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Forms for spam deletion module."""

from __future__ import absolute_import, print_function

from flask_babelex import lazy_gettext as _
from flask_wtf import Form
from wtforms import BooleanField, SubmitField


class DeleteSpamForm(Form):
    """Form for deleting spam."""

    remove_all_communities = BooleanField(
        _('Remove the user communities?'),
        default=True,
        description=_('Will remove all communities owned by the user.'),
    )

    remove_all_records = BooleanField(
        _('Remove all user records?'),
        default=True,
        description=_('Will remove all records owned by the user.'),
    )

    deactivate_user = BooleanField(
        _('Deactivate the user account?'),
        default=True,
        description=_('Will deactivate the user account.'),
    )

    delete = SubmitField(_("Permanently delete"))
