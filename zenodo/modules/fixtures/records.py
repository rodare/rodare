# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""CLI for Zenodo fixtures."""

from __future__ import absolute_import, print_function

from uuid import uuid4

from flask import current_app
from flask_security import login_user
from invenio_db import db
from invenio_sipstore.models import SIPMetadataType
from six import BytesIO

from zenodo.modules.deposit.api import ZenodoDeposit
from zenodo.modules.deposit.loaders import legacyjson_v1
from zenodo.modules.deposit.minters import zenodo_deposit_minter


def loaddemorecords(records, owner):
    """Load demo records."""
    with current_app.test_request_context():
        login_user(owner)
        for record in records:
            deposit_data = legacyjson_v1(record)
            deposit_id = uuid4()
            zenodo_deposit_minter(deposit_id, deposit_data)
            deposit = ZenodoDeposit.create(deposit_data, id_=deposit_id)
            db.session.commit()
            filename = record['files'][0]
            deposit.files[filename] = BytesIO(filename)
            db.session.commit()
            deposit.publish()
            db.session.commit()


def loadsipmetadatatypes(types):
    """Load SIP metadata types."""
    with db.session.begin_nested():
        for type in types:
            db.session.add(SIPMetadataType(**type))
    db.session.commit()
