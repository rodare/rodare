# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""CLI for Zenodo fixtures."""

from __future__ import absolute_import, print_function, unicode_literals

import hashlib
from os import makedirs, stat
from os.path import exists

from flask import current_app
from invenio_db import db
from invenio_files_rest.models import FileInstance, Location, ObjectVersion

from zenodo.modules.exporter.utils import initialize_exporter_bucket


def loadbuckets():
    """Initialize any special buckets."""
    initialize_exporter_bucket()


def loadlocations(force=False):
    """Load default file store and archive location."""
    try:
        locs = []
        uris = [
            ('default', True, current_app.config['FIXTURES_FILES_LOCATION'], ),
            ('archive', False,
             current_app.config['FIXTURES_ARCHIVE_LOCATION'], )
        ]
        for name, default, uri in uris:
            if uri.startswith('/') and not exists(uri):
                makedirs(uri)
            if not Location.query.filter_by(name=name).count():
                loc = Location(name=name, uri=uri, default=default)
                db.session.add(loc)
                locs.append(loc)

        db.session.commit()
        return locs
    except Exception:
        db.session.rollback()
        raise


def loaddemofiles(source, force=False):
    """Load demo files."""
    s = stat(source)

    with open(source, 'rb') as fp:
        m = hashlib.md5()
        m.update(fp.read())
        checksum = "md5:{0}".format(m.hexdigest())

    # Create a file instance
    with db.session.begin_nested():
        f = FileInstance.create()
        f.set_uri(source, s.st_size, checksum)

    # Replace all objects associated files.
    ObjectVersion.query.update({ObjectVersion.file_id: str(f.id)})
    db.session.commit()
