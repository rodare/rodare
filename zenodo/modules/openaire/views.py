# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Blueprint for OpenAIRE."""

from __future__ import absolute_import, print_function

from flask import Blueprint

from .helpers import openaire_link, openaire_type

blueprint = Blueprint(
    'zenodo_openaire',
    __name__
)


@blueprint.app_template_filter('openaire_link')
def link(record):
    """Generate an OpenAIRE link."""
    return openaire_link(record)


@blueprint.app_template_filter('openaire_type')
def openaire_type_filter(record):
    """Generate an OpenAIRE link."""
    return openaire_type(record)
