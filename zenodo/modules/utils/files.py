# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Files utilities."""

from __future__ import absolute_import, print_function

import sqlalchemy as sa
from flask import current_app
from invenio_files_rest.models import FileInstance


def checksum_verification_files_query():
    """Return a FileInstance query taking into account file URI prefixes."""
    files = FileInstance.query
    uri_prefixes = current_app.config.get(
        'FILES_REST_CHECKSUM_VERIFICATION_URI_PREFIXES')
    if uri_prefixes:
        files = files.filter(
            sa.or_(*[FileInstance.uri.startswith(p) for p in uri_prefixes]))
    return files
