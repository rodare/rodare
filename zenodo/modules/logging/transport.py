# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


"""Customized Sentry HTTP Transport class."""

from raven.conf import defaults
from raven.transport.http import HTTPTransport
from raven.transport.threaded import ThreadedHTTPTransport


class HTTPTransportNoVerify(HTTPTransport):
    """Transport class with disabled ssl verificaion by default."""

    def __init__(self, timeout=defaults.TIMEOUT, verify_ssl=False,
                 ca_certs=defaults.CA_BUNDLE):
        """Special constructor without ssl verification."""
        super().__init__(timeout=timeout, verify_ssl=verify_ssl,
                         ca_certs=ca_certs)


class ThreadedTransportNoVerify(ThreadedHTTPTransport):
    """Threaded transport class with disabled ssl verification by default."""

    def __init__(self, timeout=defaults.TIMEOUT, verify_ssl=False,
                 ca_certs=defaults.CA_BUNDLE):
        """Special constructor without ssl verification."""
        super().__init__(timeout=timeout, verify_ssl=verify_ssl,
                         ca_certs=ca_certs)
