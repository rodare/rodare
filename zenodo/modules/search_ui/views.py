# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Blueprint for Zenodo-Records."""

from __future__ import absolute_import, print_function

from flask import Blueprint

blueprint = Blueprint(
    'zenodo_search_ui',
    __name__,
    template_folder='templates',
    static_folder='static',
)


@blueprint.app_template_filter()
def filter_sort_options(sort_options):
    """Filters the search sort options based on the "display" key."""
    return {k: v for k, v in sort_options.items() if v.get('display', True)}
