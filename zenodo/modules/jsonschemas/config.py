# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Configuration for ZenodoJSONSchemas."""

from __future__ import absolute_import, print_function

ZENODO_JSONSCHEMAS_RECORD_SCHEMA = (
    'records/record_src-v1.0.0.json',
    'records/record-v1.0.0.json')

ZENODO_JSONSCHEMAS_DEPOSIT_SCHEMA = (
    'deposits/records/record_src-v1.0.0.json',
    'deposits/records/record-v1.0.0.json')
