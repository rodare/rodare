# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""PID Fetchers."""

from __future__ import absolute_import, print_function

from invenio_pidstore.fetchers import FetchedPID


def zenodo_record_fetcher(dummy_record_uuid, data):
    """Fetch a record's identifiers."""
    return FetchedPID(
        provider=None,
        pid_type='recid',
        pid_value=str(data['recid']),
    )


def zenodo_doi_fetcher(dummy_record_uuid, data):
    """Fetch a record's DOI."""
    return FetchedPID(
        provider=None,
        pid_type='doi',
        pid_value=str(data['doi']),
    )
