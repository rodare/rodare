# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""DoJSON based MARCXML serializer for records."""

from __future__ import absolute_import, print_function

from invenio_marc21.serializers.marcxml import MARCXMLSerializer

from .pidrelations import preprocess_related_identifiers


class ZenodoMARCXMLSerializer(MARCXMLSerializer):
    """Zenodo MARCXML serializer for records.

    Note: This serializer is not suitable for serializing large number of
    records.
    """

    def preprocess_record(self, pid, record, links_factory=None):
        """Add related identifiers from PID relations."""
        result = super(ZenodoMARCXMLSerializer, self).preprocess_record(
            pid, record, links_factory=links_factory
        )
        result = preprocess_related_identifiers(pid, record, result)
        return result
