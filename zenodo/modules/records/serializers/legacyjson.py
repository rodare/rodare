# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo Serializers."""

from __future__ import absolute_import, print_function

from flask import json
from invenio_records.api import Record

from .json import ZenodoJSONSerializer


class LegacyJSONSerializer(ZenodoJSONSerializer):
    """Legacy JSON Serializer."""

    def serialize_search(self, pid_fetcher, search_result, links=None,
                         item_links_factory=None):
        """Serialize as a json array."""
        return json.dumps([self.transform_search_hit(
            pid_fetcher(hit['_id'], hit['_source']),
            hit,
            links_factory=item_links_factory,
        ) for hit in search_result['hits']['hits']])


class DepositLegacyJSONSerializer(LegacyJSONSerializer):
    """Legacy JSON serializer.

    Dumps files directly from Bucket instead of relying on record metadata.
    """

    def preprocess_record(self, pid, record, links_factory=None):
        """Include files for single record retrievals."""
        result = super(LegacyJSONSerializer, self).preprocess_record(
            pid, record, links_factory=links_factory
        )
        if isinstance(record, Record):
            result['files'] = record.files.dumps()
        return result
