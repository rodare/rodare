# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Rodare Opendefinition customization."""

from __future__ import absolute_import, print_function

from flask import current_app


def indexer_receiver(sender, json=None, record=None, index=None,
                     **dummy_kwargs):
    """Connect to before_record_index signal to transform record for ES."""
    if index.startswith('licenses-'):
        # add recommended licenses
        rec_data = current_app.config['RODARE_APPROVED_LICENSES_DATA']
        if json['id'] in rec_data:
            json['title'] += ' (Recommended for data)'
        rec_sw = current_app.config['RODARE_APPROVED_LICENSES_SW']
        if json['id'] in rec_sw:
            json['title'] += ' (Recommended for software)'
        # Generate suggest field
        json['suggest'] = {
            'input': [json['id'], json['title']],
        }
