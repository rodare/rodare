# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Rodare Opendefinition customization."""

from invenio_indexer.signals import before_record_index
from invenio_opendefinition.indexer import \
    indexer_receiver as _indexer_receiver

from . import config
from .indexer import indexer_receiver


class RodareOpendefinition(object):
    """Rodare opendefinition class."""

    def __init__(self, app=None):
        """Extension initialization."""
        if app:
            self.init_app(app)
        self.register_signals(app)

    def init_app(self, app):
        """Flask application initialization."""
        self.init_config(app)

    @staticmethod
    def register_signals(app):
        """Register Zenodo Deposit signals."""
        before_record_index.connect(indexer_receiver, sender=app)
        before_record_index.disconnect(_indexer_receiver, sender=app)

    @staticmethod
    def init_config(app):
        """Initialize configuration."""
        for k in dir(config):
            if k.startswith('RODARE_'):
                app.config.setdefault(k, getattr(config, k))
