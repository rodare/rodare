# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Theme blueprint in order for template and static files to be loaded."""

from __future__ import absolute_import, print_function

import bleach
from flask import Blueprint

from zenodo.modules.records.serializers.fields.html import ALLOWED_ATTRS, \
    ALLOWED_TAGS

blueprint = Blueprint(
    'zenodo_theme',
    __name__,
    template_folder='templates',
    static_folder='static',
)
"""Theme blueprint used to define template and static folders."""


@blueprint.app_template_filter('sanitize_html')
def sanitize_html(value):
    """Sanitizes HTML using the bleach library."""
    return bleach.clean(
        value,
        tags=ALLOWED_TAGS,
        attributes=ALLOWED_ATTRS,
        strip=True,
    ).strip()
