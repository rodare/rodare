# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Flask-Collect filter to remove app.static_folder from source list."""

from __future__ import absolute_import, print_function

from flask import current_app


def collect_staticroot_removal(blueprints):
    """Remove collect's static root folder from list."""
    collect_root = current_app.extensions['collect'].static_root
    return [bp for bp in blueprints if (
        bp.has_static_folder and bp.static_folder != collect_root)]
