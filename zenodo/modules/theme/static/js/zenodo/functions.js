// Copyright (C) 2017-2018 HZDR
//
// This file is part of Rodare.
//
// Rodare is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rodare is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

define([], function(){
  function recordCommunityCurate() {
    var endpoint = $(this).attr('endpoint');
    var action = $(this).attr('action');
    var recid = $(this).attr('recid');
    var commId = $(this).attr('commId');
    var data = {recid: recid, action: action};
    var commContainerElemId = "#span-comm-" + commId;
    $.ajax({
      url: endpoint,
      method: "post",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(data),
      dataType: "json",
      success: function(result, status, xhr) {
        console.log("Success");
        $(commContainerElemId + " > .btn").addClass('hidden');
        $(commContainerElemId + " > ." + action + "-success-msg").removeClass('hidden')
      },
      error: function(result, status, xhr) {
        if (action == "accept" || action == "reject" || action == "remove") {
          $(commContainerElemId + " > .btn").addClass('hidden');
          $(commContainerElemId + " > .error-msg").removeClass('hidden');
        }
      }
    });
  }
  return recordCommunityCurate;
});
