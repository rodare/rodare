# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Rodare specific oauth handlers."""

from __future__ import absolute_import, print_function

from flask import current_app, flash, redirect, session, url_for
from flask_babelex import gettext as _
from flask_login import current_user
from invenio_db import db
from invenio_oauthclient.errors import OAuthClientError
from invenio_oauthclient.handlers import get_session_next_url, \
    oauth_error_handler, response_token_setter, token_getter, \
    token_session_key
from invenio_oauthclient.proxies import current_oauthclient
from invenio_oauthclient.signals import account_info_received, \
    account_setup_committed, account_setup_received
from invenio_oauthclient.utils import oauth_authenticate, oauth_get_user


@oauth_error_handler
def authorized_signup_handler(resp, remote, *args, **kwargs):
    """Handle sign-in/up functionality.
    
    :param remote: The remote application.
    :param resp: The response.
    :returns: Redirect response.
    """
    # Remove any previously stored auto register session key
    session.pop(token_session_key(remote.name) + '_autoregister', None)

    # Store token in session
    # ----------------------
    # Set token in session - token object only returned if
    # current_user.is_authenticated().
    token = response_token_setter(remote, resp)
    handlers = current_oauthclient.signup_handlers[remote.name]

    # Disallow login
    # --------------
    if not current_user.is_authenticated:
        flash(_('Login via GitHub is not permitted.'), category='danger')
        return redirect('/')

    # Setup account
    # -------------
    if not token.remote_account.extra_data:
        account_setup = handlers['setup'](token, resp)
        account_setup_received.send(
            remote, token=token, response=resp, account_setup=account_setup
        )
        db.session.commit()
        account_setup_committed.send(remote, token=token)
    else:
        db.session.commit()

    # Redirect to next
    next_url = get_session_next_url(remote.name)
    if next_url:
        return redirect(next_url)
    return redirect(url_for('invenio_oauthclient_settings.index'))
