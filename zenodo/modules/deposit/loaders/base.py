# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Loaders for records."""

from __future__ import absolute_import, print_function

from flask import current_app, has_request_context, request

from zenodo.modules.records.minters import doi_generator

from ..errors import MarshmallowErrors


def json_loader(pre_validator=None, post_validator=None, translator=None):
    """Basic JSON loader with translation and pre/post validation support."""
    def loader(data=None):
        data = data or request.json

        if pre_validator:
            pre_validator(data)
        if translator:
            data = translator(data)
        if post_validator:
            post_validator(data)

        return data
    return loader


def marshmallow_loader(schema_class, **kwargs):
    """Basic marshmallow loader generator."""
    def translator(data):
        # Replace refs when we are in request context.
        context = dict(replace_refs=has_request_context())

        # DOI validation context
        if request and request.view_args.get('pid_value'):
            managed_prefix = current_app.config['PIDSTORE_DATACITE_DOI_PREFIX']
            _, record = request.view_args.get('pid_value').data

            context['recid'] = record['recid']
            if record.has_minted_doi() or record.get('conceptdoi'):
                # if record has Zenodo DOI it's not allowed to change
                context['required_doi'] = record['doi']
            elif not record.is_published():
                context['allowed_dois'] = [doi_generator(record['recid'])]
            if record.is_published() or record.get('conceptdoi'):
                # if the record is published or this is a new version 
                # of a published record then the DOI is required
                context['doi_required'] = True
                data.setdefault('metadata', {})
                if 'doi' not in data['metadata']:
                    # if the payload doesn't contain the DOI field
                    #  for the record keep the existing one
                    data['metadata']['doi'] = record['doi']

            context['managed_prefixes'] = [managed_prefix]
            context['banned_prefixes'] = \
                ['10.5072'] if managed_prefix != '10.5072' else []

        # Extra context
        context.update(kwargs)

        # Load data
        result = schema_class(context=context).load(data)
        if result.errors:
            raise MarshmallowErrors(result.errors)
        return result.data
    return translator
