# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Deposit loaders."""

from __future__ import absolute_import, print_function

from zenodo.modules.records.serializers.schemas.json import RecordSchemaV1
from zenodo.modules.records.serializers.schemas.legacyjson import \
    LegacyRecordSchemaV1

from .base import json_loader, marshmallow_loader

# Translators
# ===========
#: JSON v1 deposit translator.
deposit_json_v1_translator = marshmallow_loader(RecordSchemaV1)
#: Legacy deposit dictionary translator.
legacyjson_v1_translator = marshmallow_loader(LegacyRecordSchemaV1)

# Loaders
# =======
#: JSON deposit record loader.
deposit_json_v1 = json_loader(
    translator=deposit_json_v1_translator,
)
#: Legacy deposit JSON record loader.
legacyjson_v1 = json_loader(
    translator=legacyjson_v1_translator,
)
