# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Query factories for deposit REST API."""


from flask import abort
from flask_security import current_user

from zenodo.modules.records.query import \
    search_factory as record_search_factory


def search_factory(*args, **kwargs):
    """Check user is logged in and then parse."""
    if not current_user.is_authenticated:
        abort(401)

    return record_search_factory(*args, **kwargs)
