// Copyright (C) 2017-2018 HZDR
//
// This file is part of Rodare.
//
// Rodare is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rodare is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

function prereserveButton($rootScope, InvenioRecordsAPI) {
  function link($scope, elem, attrs, vm) {
    $scope.prereserveDOI = function() {
      if ($scope.model.prereserve_doi &&
          $scope.model.prereserve_doi.doi) {
        $scope.model.doi = $scope.model.prereserve_doi.doi;
      } else {
        // We need to make a 'POST' call to create a deposit, or a
        // 'GET' in case we already have one, in order to get the
        // pre-reserved DOI.
        var method = angular.isUndefined(vm.invenioRecordsEndpoints.self) ? 'POST': 'GET';
        var url = vm.invenioRecordsEndpoints.self || vm.invenioRecordsEndpoints.initialization;
        $rootScope.$broadcast('invenio.records.loading.start');
        InvenioRecordsAPI.request({
          method: method,
          url: url,
          data: {},
          headers: vm.invenioRecordsArgs.headers || {}
        }).then(function success(resp) {
          if (resp.data.metadata &&
              resp.data.metadata.prereserve_doi &&
              resp.data.metadata.prereserve_doi.doi) {
            $scope.model.prereserve_doi = resp.data.metadata.prereserve_doi;
            $scope.model.doi = resp.data.metadata.prereserve_doi.doi;
          }
          $rootScope.$broadcast(
            'invenio.records.endpoints.updated', resp.data.links);
        }, function error(resp) {
          $rootScope.$broadcast('invenio.records.alert', {
            type: 'danger',
            data: resp.data,
          });
        })
        .finally(function() {
          $rootScope.$broadcast('invenio.records.loading.stop');
        });
      }
    };
  }

  return {
    scope: false,
    restrict: 'A',
    require: '^invenioRecords',
    link: link,
  };
}

prereserveButton.$inject = [
  '$rootScope',
  'InvenioRecordsAPI',
];


function communitiesSelect($http, $q, openAIRE) {
  function link($scope, elem, attrs, vm) {
    // Locals
    function initCommunities() {
      $scope.model.communities = $scope.model.communities || [];
      $scope.model.communities = $scope.model.communities.filter(
        function(comm) { return 'identifier' in comm; });
      var requests = $scope.model.communities.filter(function(comm) {
        return 'identifier' in comm;
      }).map(function(comm) {
        return $http.get('/api/communities/' + comm.identifier);
      });
      // TODO: Add a loading indicator
      $q.all(requests).then(function(fetchedCommunities) {
        $scope.communities = fetchedCommunities.map(function(res) {
          return res.data;
        }).filter(function(comm) { return 'id' in comm; });
      });
    }

    $scope.communities = [];
    $scope.communityResults = [];
    $scope.openAIRECommunities = openAIRE.communities;
    $scope.openAIRECommunitiesMapping = openAIRE.communitiesMapping;

    initCommunities();

    // Methods
    function getOpenAIRECommunities() {
      var modelCommunities = $scope.model.communities || [];
      var openaireComms = [];
      _.each(modelCommunities, function(comm) {
        if (comm.identifier in $scope.openAIRECommunitiesMapping) {
          openaireComms = openaireComms.concat($scope.openAIRECommunitiesMapping[comm.identifier]);
        }
      });
      return _.uniq(openaireComms);
    }

    $scope.refreshCommunityResults = function(data) {
      var data = data || $scope.communityResults;
      $scope.communityResults = _.filter(data, function(comm){
        return _.find($scope.communities, function(c) { return c.id == comm.id}) == undefined;
      });
    };

    $scope.searchCommunities = function(query) {
      $http.get('/api/communities/', {params: {q: query, size: 7} })
      .then(function(res){
        $scope.refreshCommunityResults(res.data.hits.hits);
      });
    }

    $scope.communityOnSelect = function(community) {
      $scope.communities.push(community)
      $scope.model.communities.push({identifier: community.id})
      $scope.refreshCommunityResults()
    };

    $scope.removeCommunity = function(commId) {
      $scope.communities = _.filter($scope.communities, function(comm){
        return comm.id !== commId;
      });
      $scope.model.communities = _.filter($scope.model.communities, function(comm){
        return comm.identifier !== commId;
      });
      // Unset the OpenAIRE subtype if it belongs to the unselected community
      var openaireComms = getOpenAIRECommunities();
      var commTypes = _.pick($scope.openAIRECommunities, openaireComms)
      var oaTypes = [];
      _.each(commTypes, function(comm) {
        _.each(comm.types[uploadType] || [], function(type) {
          oaTypes.push({ id: type.id, name: type.name, commName: comm.name })
        })
      })
      if(!($scope.model.openaire_type in oaTypes)) {
        $scope.model.openaire_type = undefined;
      }
      $scope.refreshCommunityResults()
    }
  }
  return {
    scope: false,
    restrict: 'AE',
    require: '^invenioRecords',
    link: link,
  };
}

communitiesSelect.$inject = [
  '$http',
  '$q',
  'openAIRE',
];


function openaireSubtype(openAIRE) {
  function link($scope, elem, attrs, vm) {
    // Locals
    $scope.openAIRECommunities = openAIRE.communities;
    $scope.openAIRECommunitiesMapping = openAIRE.communitiesMapping;
    $scope.vm = vm;

    // Methods
    function getOpenAIRECommunities() {
      var modelCommunities = $scope.model.communities || [];
      var openaireComms = [];
      _.each(modelCommunities, function(comm) {
        if (comm.identifier in $scope.openAIRECommunitiesMapping) {
          openaireComms = openaireComms.concat($scope.openAIRECommunitiesMapping[comm.identifier]);
        }
      });
      return _.uniq(openaireComms);
    }

    $scope.show = function() {
      var uploadType = $scope.model.upload_type;
      var openaireComms = getOpenAIRECommunities();
      var commTypes = _.pick($scope.openAIRECommunities, openaireComms)
      var res = !angular.equals(commTypes, {}) &&
      _.any(commTypes, function(comm) { return uploadType in comm.types; });
      return res;
    }

    $scope.getOptions = function() {
      var uploadType = $scope.model.upload_type;
      var openaireComms = getOpenAIRECommunities();
      var commTypes = _.pick($scope.openAIRECommunities, openaireComms)
      var options = [];
      _.each(commTypes, function(comm) {
        _.each(comm.types[uploadType] || [], function(type) {
          options.push({ id: type.id, name: type.name, commName: comm.name })
        })
      })
      return options;
    }
  }
  return {
    scope: false,
    restrict: 'AE',
    require: '^invenioRecords',
    link: link,
  };
}

openaireSubtype.$inject = [
  'openAIRE',
];

function fileBrowser($rootScope, $http, $filter, InvenioRecordsAPI) {
  function link($scope, elem, attrs, vm){
    $scope.browserLoading = false;
    $scope.dirs = [];

    // pagination
    $scope.currentPage = 0;
    $scope.pagedDirs = [];
    $scope.totalPages = 0;
    $scope.elementsPerPage = 15;

    $scope.updateFileBrowser = function(url){
      $scope.alert = '';
      if(!url){
        url = '/api/uploadbyurl/browse/' + attrs.servername;
      }
      $scope.browserLoading = true;
      var requests = $http.post(url).
        then(function(response) {
          $scope.dirs = response.data;
          $scope.search = '';
          $scope.browserLoading = false;
        }, function(response) {
          $scope.dirs = [];
          $scope.alert = response.data.message;
          $scope.search = '';
          $scope.browserLoading = false;
      }).finally(function(){
        $scope.search = '';
        $scope.filterDirs();
        $scope.browserLoading = false;
      });
      return;
    };

    $scope.pagesRange = function(){
      // always add first page
      var ret = [0];
      // add spacing page
      if($scope.currentPage > 2){
        ret.push(-1);
      }
      // add 4 pages around current_page
      for(var i=-1; i < 3; i++){
        var pageId = $scope.currentPage + i;
        if(pageId > 0 && pageId < $scope.totalPages - 1){
          ret.push(pageId);
        }
      }
      // add spacing
      var diff = Math.abs($scope.totalPages - 1 - ret[ret.length - 1]);
      if(diff > 1){
        ret.push(-1);
      }
      if(ret.indexOf($scope.totalPages - 1) < 0){
        ret.push($scope.totalPages - 1);
      }
      return ret;
    };

    $scope.createPages = function() {
      $scope.currentPage = 0;
      $scope.pagedDirs = [];
      for(var i = 0; i < $scope.filteredDirs.length; i++){
        if(i % $scope.elementsPerPage === 0){
          $scope.pagedDirs[Math.floor(i / $scope.elementsPerPage)] = [$scope.filteredDirs[i]];
        } else {
          $scope.pagedDirs[Math.floor(i / $scope.elementsPerPage)].push($scope.filteredDirs[i]);
        }
      }
      $scope.totalPages = $scope.pagedDirs.length;
    };

    $scope.filterDirs = function(){
      $scope.filteredDirs = $filter('filter')($scope.dirs, {short_path: $scope.search});
      $scope.filteredDirs = $filter('orderBy')($scope.filteredDirs, ['-isdir', 'short_path']);
      $scope.createPages();
    };

    $scope.prevPage = function() {
      if($scope.currentPage > 0){
        $scope.currentPage--;
      }
    };

    $scope.nextPage = function() {
      if($scope.currentPage < $scope.totalPages - 1){
        $scope.currentPage++;
      }
    };

    $scope.setPage = function(page) {
      if(page >= 0 && page < $scope.totalPages){
        $scope.currentPage = page;
      }
    };

    function initUploadViaSftp(endpoint, path) {
      var params = { 'path': path };
      $http({
        method: 'POST',
        url: endpoint,
        params: params
      }).then(function successCallback(resp) {
        $rootScope.$broadcast('invenio.records.alert', {
          type: 'success',
          data: { "message": resp.data.message },
        });
      }, function errorCallback(resp) {
        $rootScope.$broadcast('invenio.records.alert', {
          type: 'danger',
          data: { "message": resp.data.message },
        });
      }).finally(function () {
        $scope.fesLoading = false;
        $('#sftpModal').modal('toggle');
        $scope.browserLoading = false;
      });
    };

    $scope.uploadViaSftp = function(filepath) {
      $scope.browserLoading = true;
      var vm = $rootScope.recordsVM;
      if(!filepath){
        return;
      }
      var links = {};
      // Create deposit if it does not exist yet.
      if(angular.isUndefined(vm.invenioRecordsEndpoints.self)){
        var initUrl = vm.invenioRecordsEndpoints.initialization;
        $rootScope.$broadcast('invenio.records.loading.start');
        InvenioRecordsAPI.request({
          method: 'POST',
          url: initUrl,
          data: {},
          headers: vm.invenioRecordsArgs.headers || {}
        }).then(function success(resp) {
          $rootScope.$broadcast(
            'invenio.records.endpoints.updated', resp.data.links);
            var endpoint = resp.data.links.uploadviaurl + '/' + attrs.servername;
            initUploadViaSftp(endpoint, filepath);
        }, function error(resp) {
          $rootScope.$broadcast('invenio.records.alert', {
            type: 'danger',
            data: resp.data,
          });
        }).finally(function () {
          $rootScope.$broadcast('invenio.records.loading.stop');
        });
      } else {
        var endpoint = vm.invenioRecordsEndpoints.uploadviaurl + '/' + attrs.servername;
        initUploadViaSftp(endpoint, filepath);
      }
      return;
    }
  };
  return {
    scope: {},
    restrict: 'AE',
    link: link,
    templateUrl: '/static/templates/zenodo_deposit/filebrowser.html'
  };
}

fileBrowser.$inject = [
  '$rootScope',
  '$http',
  '$filter',
  'InvenioRecordsAPI',
]

function uploadByUrl($rootScope, $http, $filter, InvenioRecordsAPI){
  function link($scope, elem, attrs, vm){

    $scope.uploadLoading = false;

    function initUploadViaUrl(endpoint, url, key) {
      var params = { 'url': url, 'key': key };
      $http({
        method: 'POST',
        url: endpoint,
        params: params
      }).then(function successCallback(resp) {
        $rootScope.$broadcast('invenio.records.alert', {
          type: 'success',
          data: { "message": resp.data.message },
        });
      }, function errorCallback(resp) {
        $rootScope.$broadcast('invenio.records.alert', {
          type: 'danger',
          data: { "message": resp.data.message },
        });
      }).finally(function () {
        $scope.uploadLoading = false;
        $('#sftpModal').modal('toggle');
      });
    };

    $scope.uploadFile = function(url, key) {
      if(!url){
        return;
      }
      $scope.uploadLoading = true;
      var vm = $rootScope.recordsVM;
      var links = {};
      // Create deposit if it does not exist yet.
      if(angular.isUndefined(vm.invenioRecordsEndpoints.self)){
        var initUrl = vm.invenioRecordsEndpoints.initialization;
        $rootScope.$broadcast('invenio.records.loading.start');
        InvenioRecordsAPI.request({
          method: 'POST',
          url: initUrl,
          data: {},
          headers: vm.invenioRecordsArgs.headers || {}
        }).then(function success(resp) {
          $rootScope.$broadcast(
            'invenio.records.endpoints.updated', resp.data.links);
            var endpoint = resp.data.links.uploadviaurl;
            initUploadViaUrl(endpoint, url, key);
        }, function error(resp) {
          $rootScope.$broadcast('invenio.records.alert', {
            type: 'danger',
            data: resp.data,
          });
        }).finally(function () {
          $rootScope.$broadcast('invenio.records.loading.stop');
        });
      } else {
        var endpoint = vm.invenioRecordsEndpoints.uploadviaurl;
        initUploadViaUrl(endpoint, url, key);
      }

      return;
    }

  };
  return {
    scope: {},
    restrict: 'AE',
    link: link,
    templateUrl: '/static/templates/zenodo_deposit/uploadbyurl.html'
  };
}

uploadByUrl.$inject = [
  '$rootScope',
  '$http',
  '$filter',
  'InvenioRecordsAPI',
]

function convertToNumber() {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return parseInt(val, 10);
      });
      ngModel.$formatters.push(function (val) {
        return '' + val;
      });
    }
  };
}

function selectResourceTypes(resourceTypes) {
  function link($scope, elem, attrs, vm) {
    // Locals
    $scope.resourceTypesList = [{'title': 'N/A'}].concat(resourceTypes.resourceTypesList);
    $scope.vm = vm;
  }
  return {
    scope: false,
    restrict: 'AE',
    require: '^invenioRecords',
    link: link,
  };
}

selectResourceTypes.$inject = [
  'resourceTypes',
];

angular.module('invenioRecords.directives')
  .directive('prereserveButton', prereserveButton)
  .directive('communitiesSelect', communitiesSelect)
  .directive('openaireSubtype', openaireSubtype)
  .directive('fileBrowser', fileBrowser)
  .directive('uploadByUrl', uploadByUrl)
  .directive('convertToNumber', convertToNumber)
  .directive('selectResourceTypes', selectResourceTypes);
