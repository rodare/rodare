// Copyright (C) 2017-2018 HZDR
//
// This file is part of Rodare.
//
// Rodare is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rodare is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

function configZenodoDeposit($provide, decoratorsProvider, $windowProvider) {

  // TODO: Check if these are needed, since we have DEPOSIT_FORM_TEMPLATES
  // New field types
  decoratorsProvider.addMapping('bootstrapDecorator', 'uiselect', '/static/templates/zenodo_deposit/uiselect.html');
  decoratorsProvider.addMapping('bootstrapDecorator', 'uiselect_free', '/static/templates/zenodo_deposit/uiselect_free.html');
  decoratorsProvider.addMapping('bootstrapDecorator', 'ckeditor', '/static/templates/zenodo_deposit/ckeditor.html');
  decoratorsProvider.defineAddOn(
    'bootstrapDecorator',
    'grantselect',
    '/static/templates/zenodo_deposit/grantselect.html")}}'
  );
  decoratorsProvider.defineAddOn(
    'bootstrapDecorator',
    'communities',
    '/static/templates/zenodo_deposit/communities.html'
  );

  // Override invenio-records-form
  $provide.decorator('invenioRecordsFormDirective', function($delegate) {
    var directive = $delegate[0];
    var link = directive.link;
    directive.compile = function() {
      return function Link(scope, element, attrs, ctrls) {
        scope.autocompleteGrants = function(options, query) {
          // Autocomplete is triggered either from user input or change
          // of state in the model (form initialization, internal
          // modifications, etc)
          var isUserSearch = query !== '' && query !== options.scope.insideModel;
          if (query === '' && !isUserSearch) {
            query = options.scope.insideModel || '';
          }

          var query_parts = query.split('::');
          query = query_parts.pop()
          var funder = isUserSearch ?
            options.scope.funder.value
            : options.scope.form.default_funder;
          // Handle legacy FP7 grant values (only "grant_id")
          if (query_parts.length === 1) {
            funder = query_parts.pop();
          }
          options.scope.funder = {value: funder};
          options.urlParameters.funder = "'" + funder + "'";
          return scope.autocompleteSuggest(options, query)
        }

        return link.apply(this, arguments);
      };
    };
    return $delegate;
  });

 // We are injecting the record that has already been put by the jinja template in the window to prevent angular from
 // interpreting accidental angular expressions from the record's fields.
  $provide.decorator('invenioRecordsDirective', function($delegate) {
    var directive = $delegate[0];
    var link = directive.link;
    directive.compile = function() {
      return function Link(scope, element, attrs, ctrls) {
        attrs.record = JSON.stringify($windowProvider.$get().record)
        return link.apply(this, arguments);
      };
    };
    return $delegate;
  });
}

configZenodoDeposit.$inject = [
  '$provide',
  'schemaFormDecoratorsProvider',
  '$windowProvider',
];

angular.module('invenioRecords')
  .config(configZenodoDeposit);
