// Copyright (C) 2017-2018 HZDR
//
// This file is part of Rodare.
//
// Rodare is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rodare is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

// Overriding ui-select's controller to handle arrays
//
// Note: UI-Select by default doesn't play well while being inside an
// array. What happens is that if the array gets modified from the
// outside (eg. delete an array element), the changes are not picked up
// by UI-Select, and thus aren't refelcted on its internal model and
// view. This leads to inconsistent view state for the end user, eg.
// an item from the middle of a ui-select list gets removed using the
// 'X' button, but the view will still display it and remove the last
// item of the list.
//
// The remedy for this issue is to handle the update of ui-select's
// model manually, by overriding its controller.
function invenioDynamicSelectController($scope, $controller) {
  $controller('dynamicSelectController', {$scope: $scope});
  // If it is ui-select inside an array...
  if ($scope.modelArray) {
    $scope.$watchCollection('modelArray', function(newValue) {
      // If this is not the initial setting of the element...
      if (!angular.equals($scope.select_model, {})) {
        // Get the element's correct value from the array model
        var value = $scope.modelArray[$scope.arrayIndex][$scope.form.key.slice(-1)[0]];
        // Set ui-select's model to the correct value if needed
        if ($scope.insideModel !== value) {
          $scope.insideModel = value;
          var query = $scope.$eval(
            $scope.form.options.processQuery || 'query',
            { query: value }
          );
          $scope.populateTitleMap($scope.form, query);
          $scope.select_model.selected = $scope.find_in_titleMap(value);
        }
      }
    });
  }
  $scope.getSearchResults = function (form, search, pub_id) {
    // $scope.populateTitleMap(form, search);
    form.titleMap = []
    var regExpPubId = new RegExp('^[1-9]*[1-9][0-9]*$')
    return $scope.getCallback(form.options.asyncCallback)(form.options, search).then(
      function (_data) {
        // In order to work with both $http and generic promises
        _data = _data.data || _data;
        $scope.finalizeTitleMap(form, _data, form.options);
        console.log('asyncCallback items', form.titleMap);
        var addValue = true;
        for (var i = 0; i < form.titleMap.length; i++) {
          if (form.titleMap[i]['value'] == search || form.titleMap[i]['value'] == pub_id) {
            addValue = false;
            break;
          }
        }
        if (addValue) {
          if (search == "" && typeof pub_id != 'undefined') {
            form.titleMap.unshift({ "metadata.title": "Manual entry of ROBIS publication ID.", "metadata.pub_id": pub_id, "value": pub_id, "name": "Manual entry of ROBIS publication ID." });
          } else {
            if (search.match(regExpPubId)) {
              form.titleMap.unshift({ "metadata.title": "Manual entry of ROBIS publication ID.", "metadata.pub_id": search, "value": search, "name": "Manual entry of ROBIS publication ID." });
            }
          }
        }
      },
      function (data, status) {
        if (form.options.onPopulationError) {
          $scope.getCallback(form.options.onPopulationError)(form, data, status);
        }
        else {
          alert("Loading select items failed(Options: '" + String(form.options) +
            "\nError: " + status);
        }
      });
  };
}

function importMetadataButton($scope, $rootScope, InvenioRecordsAPI) {
  $scope.importMetadata = function () {
    $scope.content = {
      "is_imported": false,
      "import_failed": false
    };
    var pub_id = $scope.model.pub_id;
    var regExpDocId = new RegExp('^[1-9]$');
    var regExpPubId = new RegExp('^[1-9]*[1-9][0-9]*$')
    if (!pub_id.match(regExpPubId)) {
      $rootScope.$broadcast('invenio.records.alert', {
        type: 'danger',
        data: { "message": "Please specify the ROBIS publication id according to the required scheme. It is a positive number, e.g. 1234." },
      });
      return;
    }
    if (!$scope.model.doc_id) {
      $scope.model.doc_id = '1';
    }
    var doc_id = $scope.model.doc_id;
    if (!doc_id.match(regExpDocId)) {
      $rootScope.$broadcast('invenio.records.alert', {
        type: 'danger',
        data: { "message": "Please correct the ROBIS document id. It must be in the range 1 to 9." },
      });
      return;
    }
    var url = '/api/robis/HZDR-pub_id:doc_id'
    url = url.replace("pub_id", pub_id);
    url = url.replace("doc_id", doc_id)
    $rootScope.$broadcast('invenio.records.loading.start');
    // make post request to retrieve metadata from ROBIS
    InvenioRecordsAPI.request({
      method: 'GET',
      url: url,
      data: {},
      headers: {}
    }).then(function success(resp) {
      if (resp.data.metadata.published) {
        var msg = { "message": "Your upload is already published in ROBIS." };
        $rootScope.$broadcast('invenio.records.alert', {
          type: 'danger',
          data: msg,
        });
        $scope.content.import_failed = true;
        return;
      }
      $scope.model.title = resp.data.metadata.title;
      $scope.model.description = resp.data.metadata.description;
      $scope.model.creators = resp.data.metadata.creators;
      $scope.model.keywords = resp.data.metadata.keywords;
      $scope.model.related_identifiers = resp.data.metadata.related_identifiers;
      $scope.model.doc_id = resp.data.metadata.doc_id;
      $scope.content.is_imported = true;
    }, function error(resp) {
      // TODO: if retrieval failed, try importing from ROBIS
      var url = '/api/record/robis_import/pub_id/doc_id';
      url = url.replace("pub_id", pub_id);
      url = url.replace("doc_id", doc_id);
      InvenioRecordsAPI.request({
        method: 'POST',
        url: url,
        data: {},
        headers: {}
      }).then(function success(resp) {
        $scope.model.title = resp.data.metadata.title;
        $scope.model.description = resp.data.metadata.description;
        $scope.model.creators = resp.data.metadata.creators;
        $scope.model.keywords = resp.data.metadata.keywords;
        $scope.model.related_identifiers = resp.data.metadata.related_identifiers;
        $scope.content.is_imported = true;
      }, function error(resp) {
        $scope.content.import_failed = true;
        $rootScope.$broadcast('invenio.records.alert', {
          type: 'danger',
          data: resp.data,
        });
      });
    }).finally(function () {
      $rootScope.$broadcast('invenio.records.loading.stop');
    });
    return;
  }
}

invenioDynamicSelectController.$inject = [
  '$scope',
  '$controller',
];

importMetadataButton.$inject = [
  '$scope',
  '$rootScope',
  'InvenioRecordsAPI',
];

angular.module('schemaForm')
  .controller('invenioDynamicSelectController', invenioDynamicSelectController)
  .controller('importMetadataButton', importMetadataButton)
