# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

.pg10-services: &pg10-services
    services:
        - redis:3-alpine
        - rabbitmq:3-alpine
        - name: docker.elastic.co/elasticsearch/elasticsearch-oss:7.4.2
          alias: elasticsearch
          command: [ "bin/elasticsearch", "-Ediscovery.type=single-node" ]
        - postgres:10-alpine

.python36: &python36
    image: python:3.6-buster

stages:
    - test
    - coverage
    - deploy

variables:
    POSTGRES_USER: postgres
    POSTGRES_DB: test
    POSTGRES_PASSWORD: password
    SQLALCHEMY_DATABASE_URI: "postgresql+psycopg2://postgres:password@postgres:5432/test"
    PIP_CACHE_DIR: pip-cache
    VIRTUAL_ENV: "/usr/local"
    CACHE_REDIS_HOST: redis
    ACCOUNTS_SESSION_REDIS_URL: "redis://redis:6379/0"
    CELERY_RESULT_BACKEND: "redis://redis:6379/0"
    BROKER_URL: "amqp://guest:guest@rabbitmq:5672//"
    CACHE_REDIS_URL: "redis://redis:6379/0"
    DISPLAY: ":99.0"
    E2E_WEBDRIVER_BROWSERS: "Firefox"
    INVENIO_SEARCH_ELASTIC_HOSTS: "[{'host': 'elasticsearch', 'timeout': 60}]"

before_script:
    - git config --global url."https://".insteadOf git://
    - apt-get update
    - apt-get install -qy --fix-missing --no-install-recommends libxml2-dev libxmlsec1-dev build-essential libfreetype6-dev
    - curl -sL https://deb.nodesource.com/setup_8.x | bash -
    - apt-get -qy install --fix-missing --no-install-recommends nodejs npm
    - pip install --upgrade pip setuptools==45.3.0 py
    - pip install wheel coveralls requirements-builder
    - ./scripts/setup-npm.sh
    - cat requirements.txt > .ci-prod-requirements.txt
    - requirements-builder -e all,postgresql --level=pypi setup.py > .ci-latest-requirements.txt

.only: &only
    only:
        - master
        - /^deploy-qa-.*$/
        - /^deploy-production-.*$/
        - merge_requests

.artifacts: &artifacts
    artifacts:
        reports:
            junit: ${CI_PROJECT_DIR}/report.xml
        paths:
            - ${CI_PROJECT_DIR}/.coverage.${CI_JOB_ID}
        expire_in: 1 day
        when: on_success

.prod: &prod
    stage: test
    cache:
        key: prod3
        paths:
            - pip-cache
            - $HOME/.nvm
    script:
        - pip install -r .ci-prod-requirements.txt
        - pip install git+https://gitlab-ci-token:$CI_BUILD_TOKEN@codebase.helmholtz.cloud/rodare/rodare-robis.git@master#egg=rodare-robis[elasticsearch7]
        - pip install -e .[all,postgresql]
        - ./scripts/setup-assets.sh
        - ./run-tests.sh
    after_script:
        - mv .coverage .coverage.${CI_JOB_ID}
    <<: *artifacts
    except:
        - production
        - qa
    <<: *only

.latest: &latest
    stage: test
    cache:
        key: latest3
        paths:
            - pip-cache
            - $HOME/.nvm
    script:
        - echo "-e git+https://codebase.helmholtz.cloud/rodare/invenio-uploadbyurl.git@master#egg=invenio-uploadbyurl" >> .ci-latest-requirements.txt
        - pip install -r .ci-latest-requirements.txt
        - pip install git+https://gitlab-ci-token:$CI_BUILD_TOKEN@codebase.helmholtz.cloud/rodare/rodare-robis.git@master#egg=rodare-robis[elasticsearch7]
        - pip install -e .[all,postgresql]
        - ./scripts/setup-assets.sh
        - ./run-tests.sh
    after_script:
        - mv .coverage .coverage.${CI_JOB_ID}
    <<: *artifacts
    allow_failure: true
    except:
        - production
        - qa
    <<: *only

.e2e-prod: &e2e-prod
    stage: test
    cache:
        key: prod3
        paths:
            - pip-cache
            - $HOME/.nvm
    script:
        - apt-get install -qy --fix-missing --no-install-recommends xvfb firefox-esr
        - Xvfb $DISPLAY &
        - wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz; mkdir geckodriver; tar -xzf geckodriver-v0.26.0-linux64.tar.gz -C geckodriver; export PATH=$PATH:$PWD/geckodriver;
        - pip install -r .ci-prod-requirements.txt
        - pip install git+https://gitlab-ci-token:$CI_BUILD_TOKEN@codebase.helmholtz.cloud/rodare/rodare-robis.git@master#egg=rodare-robis[elasticsearch7]
        - pip install -e .[all,postgresql]
        - ./scripts/setup-assets.sh
        - py.test tests/e2e
    after_script:
        - mv .coverage .coverage.${CI_JOB_ID}
    <<: *artifacts
    except:
        - production
        - qa
    <<: *only

global-coverage:
    image: python:3.6-alpine
    stage: coverage
    before_script:
        - echo "Skip before_script section."
    script:
        - pip install coverage
        - coverage combine
        - coverage report -m
    after_script:
        - echo "Skip after_script section."
    coverage: '/^TOTAL\s+\d+\s+\d+\s+(\d+\%)$/'
    <<: *only

py3.6-prod-pg10:
    <<: *prod
    <<: *python36
    <<: *pg10-services

py3.6-latest-pg10:
    <<: *latest
    <<: *python36
    <<: *pg10-services

# py3.6-prod-e2e-pg10:
#     <<: *e2e-prod
#     <<: *python36
#     <<: *pg10-services

.common-deploy: &common-deploy
    stage: deploy
    image: python:3.6
    variables:
        GIT_STRATEGY: none
    before_script:
        - apt-get update -y && apt-get install -y rsync
        # Install ssh-agent if not already installed, it is required by Docker.
        # (change apt-get to yum if you use a CentOS-based image)
        - 'which ssh-agent || ( apt-get install openssh-client -y )'

        # Run ssh-agent (inside the build environment)
        - eval $(ssh-agent -s)

        # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
        - ssh-add <(echo "$SSH_PRIVATE_KEY")

        # In order to properly check the server's host key, assuming you created the
        # SSH_SERVER_HOSTKEYS variable previously, uncomment the following two lines
        # instead.
        - mkdir -p ~/.ssh
        - '[[ -f /.dockerenv ]] && echo "$SSH_SERVER_HOSTKEYS" > ~/.ssh/known_hosts'
        - pip install --upgrade pip setuptools==45.3.0 pipenv
        - git clone -b v1.1.0 --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@codebase.helmholtz.cloud/rodare/rodare-ansible.git
        - cd rodare-ansible/
        - env --unset=VIRTUAL_ENV pipenv install
    when: manual
    allow_failure: false
    tags:
        - internal
    # Remove, if there is a solution for:
    # https://gitlab.com/gitlab-org/gitlab-ee/issues/6144
    dependencies: []


deploy_to_production:
    <<: *common-deploy
    script:
        - env --unset=VIRTUAL_ENV pipenv run ansible-playbook -i environments/production/hosts -e "rodare_version=${CI_COMMIT_TAG} ansible_python_interpreter=/usr/bin/python3" playbooks/rolling_upgrade.yml
    environment:
        name: production
        url: https://rodare.hzdr.de
    only:
        - /^deploy-production-.*$/

deploy_to_qa:
    <<: *common-deploy
    script:
        - env --unset=VIRTUAL_ENV pipenv run ansible-playbook -i environments/qa/hosts -e "rodare_version=${CI_COMMIT_TAG} ansible_python_interpreter=/usr/bin/python3" playbooks/rolling_upgrade.yml
    environment:
        name: qa
        url: https://rodare-test.hzdr.de
    only:
        - /^deploy-qa-.*$/
