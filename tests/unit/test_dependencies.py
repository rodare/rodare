# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Basics tests to ensure DB and Elasticsearch is running."""

from invenio_search import current_search, current_search_client


def test_es_running(app):
    """Test search view."""
    assert current_search_client.ping()


def test_es_state(app, es):
    """Test generated mappings, templates and aliases on ElasticSearch."""
    prefix = app.config['SEARCH_INDEX_PREFIX']
    suffix = current_search._current_suffix

    assert current_search_client.indices.get_alias() == {
            prefix + 'grants-grant-v1.0.0' + suffix: {
                'aliases': {
                    prefix + 'grants': {},
                    prefix + 'grants-grant-v1.0.0': {}
                }
            },
            prefix + 'records-record-v1.0.0' + suffix: {
                'aliases': {
                    prefix + 'records': {},
                    prefix + 'records-record-v1.0.0': {}
                }
            },
            prefix + 'deposits-records-record-v1.0.0' + suffix: {
                'aliases': {
                    prefix + 'deposits-records-record-v1.0.0': {},
                    prefix + 'deposits': {},
                    prefix + 'deposits-records': {}
                }
            },
            prefix + 'licenses-license-v1.0.0' + suffix: {
                'aliases': {
                    prefix + 'licenses-license-v1.0.0': {},
                    prefix + 'licenses': {}
                }
            },
            prefix + 'funders-funder-v1.0.0' + suffix: {
                'aliases': {
                    prefix + 'funders': {},
                    prefix + 'funders-funder-v1.0.0': {}
                }
            },
            prefix + 'robis-robis-v1.0.0' + suffix: {
                'aliases': {
                    prefix + 'robis': {},
                    prefix + 'robis-robis-v1.0.0': {}
                }
            },
        }

    templates = {
        k: (set(v['index_patterns']), set(v['aliases'].keys()))
        for k, v in current_search_client.indices.get_template().items()
    }
    assert templates == {
        prefix + 'record-view-v1.0.0': (
            {prefix + 'events-stats-record-view-*'},
            {prefix + 'events-stats-record-view'},
        ),
        prefix + 'file-download-v1.0.0': (
            {prefix + 'events-stats-file-download-*'},
            {prefix + 'events-stats-file-download'},
        ),
        prefix + 'aggr-record-view-v1.0.0': (
            {prefix + 'stats-record-view-*'},
            {prefix + 'stats-record-view'},
        ),
        prefix + 'aggr-record-download-v1.0.0': (
            {prefix + 'stats-file-download-*'},
            {prefix + 'stats-file-download'},
        ),
    }
