# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Unit tests Zenodo JSON schemas."""

from __future__ import absolute_import, print_function

from zenodo.modules.jsonschemas.compilers import compile_deposit_jsonschema, \
    compile_record_jsonschema
from zenodo.modules.jsonschemas.utils import resolve_schema_path


def test_compile_schemas(app):
    """Test record jsonschema compilation.

    NOTE: Failure of this test most likely means that the 'compiled'
    jsonschemas have been edited manually and are divergent from the
    'source' jsonschemas.
    """
    config_vars = [
        app.config['ZENODO_JSONSCHEMAS_RECORD_SCHEMA'],
        app.config['ZENODO_JSONSCHEMAS_DEPOSIT_SCHEMA'],
    ]
    compile_funcs = [
        compile_record_jsonschema,
        compile_deposit_jsonschema,
    ]
    for config_var, compile_func in zip(config_vars, compile_funcs):
        src, dst = config_var
        compiled_schema = compile_func(src)
        repo_schema = resolve_schema_path(dst)
        assert compiled_schema == repo_schema
