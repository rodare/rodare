# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare. If not, see <http://www.gnu.org/licenses/>.

"""Unit test for Zenodo json serializer."""

import json

import pytest
from flask import current_app, url_for
from helpers import login_user_via_session


@pytest.mark.parametrize('user_info, file_info_visible', [
    # anonymous user
    (None, False),
    # owner
    (dict(email='info@zenodo.org', password='tester'), True),
    # not owner
    (dict(email='test@zenodo.org', password='tester2'), False),
    # admin user
    (dict(email='admin@zenodo.org', password='admin'), True),
])
def test_closed_access_record_serializer(api, users, json_headers,
                                         closed_access_record,
                                         user_info, file_info_visible):
    """Test closed access record serialisation using records API."""
    with api.test_request_context():
        with api.test_client() as client:
            if user_info:
                # Login as user
                login_user_via_session(client, email=user_info['email'])
            res = client.get(
                url_for('invenio_records_rest.recid_item',
                        pid_value=closed_access_record['recid']),
                headers=json_headers
            )
            r = json.loads(res.data.decode('utf-8'))

            assert (r['links'].get('bucket') is not None) == file_info_visible
            assert (r.get('files') is not None) == file_info_visible


@pytest.mark.parametrize('user_info', [
    # anonymous user
    None,
    # owner
    dict(email='info@zenodo.org', password='tester'),
    # not owner
    dict(email='test@zenodo.org', password='tester2'),
    # admin user
    dict(email='admin@zenodo.org', password='admin'),
])
def test_closed_access_record_search_serializer(
        api, users, json_headers, user_info, closed_access_record):
    """Test closed access record serialisation of the search result."""
    with api.test_request_context():
        with api.test_client() as client:
            if user_info:
                # Login as user
                login_user_via_session(client, email=user_info['email'])
            res = client.get(
                url_for('invenio_records_rest.recid_list'),
                headers=json_headers
            )

            r = json.loads(res.data.decode('utf-8'))
            assert r[0]['links'].get('bucket', None) is None
            assert len(r[0].get('files', [])) == 0


def test_record_thumbnails_serializer(api, record_with_image_creation):
    """Test closed access record serialisation using records API."""
    pid, record, record_url = record_with_image_creation
    cached_thumbnails = current_app.config['CACHED_THUMBNAILS']
    with api.test_request_context():
        with api.test_client() as client:
            res = client.get(url_for(
                'invenio_records_rest.recid_item', pid_value=pid.pid_value))
        for thumbnail in cached_thumbnails:
            assert res.json['links']['thumbs'][thumbnail] == \
                'http://localhost/record/12345/thumb{}'.format(thumbnail)
        assert res.json['links']['thumb250']
