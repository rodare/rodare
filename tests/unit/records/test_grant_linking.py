# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Grant linking tests."""

from __future__ import absolute_import, print_function

from flask import current_app
from invenio_records.api import Record


def test_grant_linking(app, db, minimal_record, grant_records):
    """Test grant linking."""
    minimal_record['grants'] = [{
        '$ref': 'http://rodare.hzdr.de/grants/10.13039/501100000780::282896'}]
    record = current_app.extensions['invenio-records'].replace_refs(
        Record.create(minimal_record))
    assert record['grants'][0]['funder']['name'] == 'European Commission'
    record.validate()
