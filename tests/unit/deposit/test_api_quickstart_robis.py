# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test Zenodo deposit Quckstart."""

from __future__ import absolute_import, print_function, unicode_literals

import json

import httpretty
import pytest
from flask import url_for
from invenio_search import current_search
from six import BytesIO

from zenodo.modules.deposit.api import ZenodoDeposit
from zenodo.modules.deposit.errors import AlreadyPublishedError


def test_zenodo_quickstart_workflow(robis_api, db, es, locations, write_token,
                                    json_auth_headers, license_record):
    """Test zenodo quickstart workflow."""
    api = robis_api
    robis_base_url = api.config['RODARE_ROBIS_ENDPOINT_URL']
    robis_response = dict(
        pub_id=27252,
        doc_id=1,
        metadata=dict(
            type="FDR",
            rodaretype=1,
            creators=[
                dict(
                    name="Doe, John",
                    affiliation="Zenodo"
                )
            ],
            title="My first upload",
            description="This is my first upload"
        ),
        approved=True,
        published=True,
        writeperms=["user1"],
        readperms=["<public>"]
    )

    with api.test_request_context():
        with api.test_client() as client:
            # Try get deposits as anonymous user
            res = client.get(url_for('invenio_deposit_rest.depid_list'))
            assert res.status_code == 401

            # Try get deposits as logged-in user
            res = client.get(
                url_for('invenio_deposit_rest.depid_list'),
                headers=json_auth_headers
            )
            assert res.status_code == 200
            data = json.loads(res.get_data(as_text=True))
            assert data == []

            # Create a new deposit
            res = client.post(
                url_for('invenio_deposit_rest.depid_list'),
                headers=json_auth_headers,
                data=json.dumps({})
            )
            assert res.status_code == 201
            data = json.loads(res.get_data(as_text=True))
            deposit_id = data['id']
            assert data['files'] == []
            assert data['title'] == ''
            assert 'created' in data
            assert 'modified' in data
            assert 'id' in data
            assert 'metadata' in data
            assert 'doi' not in data
            assert data['state'] == 'unsubmitted'
            assert data['owner'] == write_token['token'].user_id

            current_search.flush_and_refresh(index='deposits')

            # Upload a file
            files = {'file': (BytesIO(b'1, 2, 3'), "myfirstfile.csv"),
                     'name': 'myfirstfile.csv'}
            res = client.post(
                data['links']['files'],
                headers=json_auth_headers,
                data=files,
                content_type='multipart/form-data',
            )
            assert res.status_code == 201
            data = json.loads(res.get_data(as_text=True))
            assert data['checksum'] == '66ce05ea43c73b8e33c74c12d0371bc9'
            assert data['filename'] == 'myfirstfile.csv'
            assert data['filesize'] == 7
            assert data['id']

            # modify deposit
            deposit = {
                "metadata": {
                    "title": "My first upload",
                    "upload_type": "poster",
                    "description": "This is my first upload",
                    "creators": [
                        {
                            "name": "Doe, John",
                            "affiliation": "Zenodo"
                        }
                    ]
                }
            }
            # Publish deposit
            httpretty.enable()
            # Mock ROBIS response
            httpretty.register_uri(
                httpretty.GET,
                api.config['RODARE_ROBIS_SET_ID_URL'],
                body={},
            )
            httpretty.register_uri(
                httpretty.GET,
                api.config['RODARE_ROBIS_UPDATE_URL'],
                body={},
            )
            res = client.put(
                url_for('invenio_deposit_rest.depid_item',
                        pid_value=deposit_id),
                headers=json_auth_headers,
                data=json.dumps(deposit)
            )
            assert res.status_code == 200

            # Publish deposit, ROBIS side already published
            res = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='publish'),
                headers=json_auth_headers,
            )
            assert res.status_code == 400
            assert b'Please specify your ROBIS publication ID.' in res.data

            # add pub_id
            deposit['metadata']['pub_id'] = '27252'
            # Mock ROBIS response
            httpretty.register_uri(
                httpretty.GET,
                api.config['RODARE_ROBIS_SET_ID_URL'],
                body={},
            )
            httpretty.register_uri(
                httpretty.GET,
                api.config['RODARE_ROBIS_UPDATE_URL'],
                body={},
            )
            res = client.put(
                url_for('invenio_deposit_rest.depid_item',
                        pid_value=deposit_id),
                headers=json_auth_headers,
                data=json.dumps(deposit)
            )
            assert res.status_code == 200

            # Mock ROBIS response
            httpretty.register_uri(
                httpretty.GET,
                robis_base_url,
                content_type='application/json',
                body=json.dumps(robis_response),
            )
            # Publish deposit, ROBIS side already published
            res = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='publish'),
                headers=json_auth_headers,
            )
            assert res.status_code == 400
            assert b'Your upload is already published in ROBIS.' in res.data

            # Publish deposit, ROBIS wrong username
            robis_response['published'] = False
            robis_response['writeperms'] = ['user2']
            httpretty.register_uri(
                httpretty.GET,
                robis_base_url,
                content_type='application/json',
                body=json.dumps(robis_response),
            )
            res = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='publish'),
                headers=json_auth_headers,
            )
            assert res.status_code == 400
            assert (b'You do not have write permissions for this upload.'
                    in res.data)

            # Publish deposit, ROBIS wrong title
            robis_response['writeperms'] = ['user2', 'user1']
            robis_response['metadata']['title'] = 'Test title'
            httpretty.register_uri(
                httpretty.GET,
                robis_base_url,
                content_type='application/json',
                body=json.dumps(robis_response),
            )
            res = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='publish'),
                headers=json_auth_headers,
            )
            assert res.status_code == 400
            assert b'Title and description do not match.' in res.data

            # Publish deposit, not approved in ROBIS
            robis_response['metadata']['title'] = 'My first upload'
            robis_response['approved'] = False
            # For now, we want to publish on Rodare without approval from Robis
            # TODO: Unquote when approval process on Rodare is implemented.
            # httpretty.register_uri(
            #     httpretty.GET,
            #     robis_base_url,
            #     content_type='application/json',
            #     body=json.dumps(robis_response),
            # )
            # res = client.post(
            #     url_for('invenio_deposit_rest.depid_actions',
            #             pid_value=deposit_id, action='publish'),
            #     headers=json_auth_headers,
            # )
            # assert res.status_code == 400
            # assert b'Your record is not approved in ROBIS.' in res.data

            # Finally publish deposit
            robis_response['approved'] = True
            httpretty.register_uri(
                httpretty.GET,
                robis_base_url,
                content_type='application/json',
                body=json.dumps(robis_response),
            )
            httpretty.register_uri(
                httpretty.GET,
                api.config['RODARE_ROBIS_UPDATE_URL'],
                body={},
            )
            httpretty.register_uri(
                httpretty.GET,
                api.config['RODARE_ROBIS_PUBLISH_URL'],
                body={},
            )
            res = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='publish'),
                headers=json_auth_headers,
            )
            assert res.status_code == 202

            httpretty.disable()
            recid = json.loads(res.get_data(as_text=True))['record_id']

            # Check that record exists.
            current_search.flush_and_refresh(index='records')
            res = client.get(url_for(
                'invenio_records_rest.recid_item', pid_value=recid))
            assert res.status_code == 200
            data = json.loads(res.get_data(as_text=True))

            # Assert that a DOI has been assigned.
            assert data['doi'] == '10.5072/zenodo.{0}'.format(recid)


@httpretty.activate
def test_triggerupdate(app, api, db, deposit, robis_entry, json_auth_headers):
    """Test zenodo triggerupdate action."""
    deposit.files['foobar.txt'] = BytesIO(b'Hello World!')
    db.session.commit()
    assert deposit['title'] == 'Test title'

    endpoint_url = app.config['RODARE_ROBIS_ENDPOINT_URL']
    httpretty.register_uri(
        httpretty.GET,
        endpoint_url,
        content_type='application/json',
        body=json.dumps(robis_entry)
    )

    # Triggerupdate only add robis id if not published
    pub_id = robis_entry['pub_id']
    doc_id = robis_entry['doc_id']
    deposit_id = deposit['_deposit']['id']
    with api.test_request_context():
        with api.test_client() as client:
            response = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='triggerupdate'),
                query_string={'pub_id': pub_id, 'doc_id': doc_id},
                headers=json_auth_headers
            )

            data = json.loads(response.get_data(as_text=True))
            new_title = robis_entry['metadata']['title']

            assert response.status_code == 201
            assert data['title'] == 'Test title'
            assert data['metadata']['title'] == 'Test title'
            assert len(data['metadata']['related_identifiers']) == 1
            assert data['metadata']['pub_id'] == str(pub_id)

            # After publishing all other metadata gets updated
            response = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='publish'),
                headers=json_auth_headers
            )
            response = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='triggerupdate'),
                query_string={'pub_id': pub_id, 'doc_id': doc_id},
                headers=json_auth_headers
            )
            data = json.loads(response.get_data(as_text=True))

            assert response.status_code == 201
            assert data['title'] == new_title
            assert data['metadata']['title'] == new_title

            # Triggerupdate returns 409 if unpublished changes exists
            response = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        pid_value=deposit_id, action='edit'),
                headers=json_auth_headers
            )
            response = client.post(
                url_for('invenio_deposit_rest.depid_actions',
                        action='triggerupdate',
                        pid_value=deposit['_deposit']['id']),
                query_string={'pub_id': pub_id, 'doc_id': doc_id},
                headers=json_auth_headers
            )

            assert response.status_code == 409
            assert b'The deposit has unpublished changes' in response.data
