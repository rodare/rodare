# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test Zenodo deposit utils."""

from __future__ import absolute_import, print_function

from zenodo.modules.deposit.utils import suggest_language


def test_suggest_language():
    """Test language suggestions."""
    s = suggest_language('pl')
    assert len(s) == 1
    assert s[0].alpha_3 == 'pol'
    # 'Northern Sami' doesn't contain 'sme' substring but should be first
    # in suggestions, since 'sme' is its ISO 639-2 code.
    s = suggest_language('sme')
    assert len(s) > 1  # More than one result
    assert s[0].alpha_3 == 'sme'
    assert 'sme' not in s[0].name.lower()
    assert 'sme' in s[1].name.lower()  # Second result matched by name

    s = suggest_language('POLISH')
    assert s[0].alpha_3 == 'pol'

    # lower-case
    s = suggest_language('polish')
    assert s[0].alpha_3 == 'pol'
