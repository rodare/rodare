# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo fixtures CLI test cases."""

from __future__ import absolute_import, print_function

from click.testing import CliRunner
from invenio_records.models import RecordMetadata

from zenodo.modules.fixtures.cli import loadfp6grants_cli, loadfunders_cli


def test_loadfunders_and_fp6grants(script_info, db):
    """Test loading of funders fixture and FP6 grants."""
    assert not RecordMetadata.query.count()
    runner = CliRunner()
    res = runner.invoke(loadfunders_cli, [], obj=script_info)
    assert res.exit_code == 0
    # We support 14 funders, but 4 parent funders are also required to properly
    # index the funder records.
    assert RecordMetadata.query.count() == 18

    res = runner.invoke(loadfp6grants_cli, [], obj=script_info)
    assert res.exit_code == 0
    assert RecordMetadata.query.count() == 20  # + 2 FP6 grants
