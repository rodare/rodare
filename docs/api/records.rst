..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


Records
-------

.. automodule:: zenodo.modules.records
   :members:
   :undoc-members:

Models
~~~~~~

.. automodule:: zenodo.modules.records.models
   :members:
   :undoc-members:

Serializers
~~~~~~~~~~~

.. automodule:: zenodo.modules.records.serializers
   :members:
   :undoc-members:

Fetchers and minters
~~~~~~~~~~~~~~~~~~~~

.. automodule:: zenodo.modules.records.minters
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.records.fetchers
   :members:
   :undoc-members:

Indexing
~~~~~~~~

.. automodule:: zenodo.modules.records.indexer
   :members:
   :undoc-members:

Permissions
~~~~~~~~~~~

.. automodule:: zenodo.modules.records.permissions
   :members:
   :undoc-members:


Tasks
~~~~~

.. automodule:: zenodo.modules.records.tasks
   :members:
   :undoc-members:


Views
~~~~~

.. automodule:: zenodo.modules.records.views
   :members:
   :undoc-members:


Marshmallow schemas
~~~~~~~~~~~~~~~~~~~

.. automodule:: zenodo.modules.records.serializers.schemas.datacite
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.records.serializers.schemas.dc
   :members:
   :undoc-members:


.. automodule:: zenodo.modules.records.serializers.schemas.json
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.records.serializers.schemas.legacyjson
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.records.serializers.schemas.marc21
   :members:
   :undoc-members:

Marshmallow fields
~~~~~~~~~~~~~~~~~~

.. automodule:: zenodo.modules.records.serializers.fields.datetime
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.records.serializers.fields.doi
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.records.serializers.fields.persistentid
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.records.serializers.fields.trimmedstring
   :members:
   :undoc-members:
