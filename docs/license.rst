License
=======

.. note::
   Rodare source code is licensed under GNU General Public License version 3.

Copyright (C) 2017-2018 HZDR

This file is part of Rodare.

Rodare is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Rodare is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


Documentation
-------------
The Rodare source code documentation is licensed under `Creative Commons Attribution
4.0 International (CC-BY) <https://creativecommons.org/licenses/by/4.0/>`_

Logos
-----
Logos in the directory ``zenodo/modules/theme/static/img/`` is property of
their respective owners. If you want to use any of them, please seek explicit
permissions from the owner.
