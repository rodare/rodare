# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

import time

from fabric import task
from invoke import Exit

environment = dict(
    INVENIO_INSTANCE_PATH='/opt/rodare/var/instance',
)


@task
def update_repo(c, version):
    """Update repo to the specified version."""
    if not version:
        raise Exit('Specify a version')
    with c.cd('/code/rodare'):
        c.run('git fetch origin')
        c.run('git reset --hard HEAD')
        c.run('git checkout {version}'.format(version=version))
        c.run('chown -R rodare:rodare /code/rodare')


@task
def deploy(c):
    """Automatically deploy RODARE."""
    with c.cd('/code/rodare'):
        with c.prefix('source /opt/rodare/bin/activate'):
            c.run('pip install -r requirements.txt')
            c.run('pip install -e .[postgresql,elasticsearch2]')
            c.run('python -O -m compileall .')
            c.run('invenio npm --pinned-file package.pinned.json',
                  env=environment)
            c.run(
                'cd {0}/static && npm install'.format(
                    environment['INVENIO_INSTANCE_PATH']),
                env=environment
            )
            c.run('invenio collect -v', env=environment)
            c.run('invenio assets build', env=environment)
            c.run('chown -R rodare:rodare /opt/rodare /code/rodare')


@task
def restart_rodare(c):
    """Restart the RODARE services on all machines."""
    c.run('service rodare reload')
    c.run('service rodare-worker restart')
    # Sleep 30 sec to allow node to be up again
    time.sleep(30)


@task
def update_mathjax(c, version):
    """Update local MathJax to specific version."""
    if not version:
        raise Exit('Please specify the version you wish to install.')
    with c.cd('/opt/rodare/var/instance/static/MathJax'):
        c.run('git fetch origin {version}'.format(version=version))
        c.run('git reset --hard HEAD')
        c.run('git checkout {version}'.format(version=version))
        c.run('chown -R rodare:rodare /opt/rodare/var/instance/static/MathJax')
