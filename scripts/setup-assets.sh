#!/usr/bin/env bash
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

set -e

CWD=`pwd`
SOURCE=${1:-"package.pinned.json"}

# Checking binaries
if [[ -z "$(which cleancss)" || -z "$(which node-sass)" || -z "$(which uglifyjs)" || -z "$(which r_js)" ]]
then
    echo "Please run ./setup-npm"; exit 1;
fi

zenodo npm --pinned-file ${SOURCE}
cd ${VIRTUAL_ENV}/var/instance/static
rm -rf gen .webassets-cache
npm install
cd ${CWD}
zenodo collect -v
zenodo assets build
